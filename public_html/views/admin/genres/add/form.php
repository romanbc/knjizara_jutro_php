<form name="formAdd" method="POST" action="modules/add.php">
    <p class="text-primary">Dodavanje novog žanra</p>
    <div class="form-group">
        <input type="text" class="form-control name-100"  name="tbName"
            placeholder="Unesite naziv" />
    </div>

    <input type="hidden" name="what" value="genres" />
    <input type="reset" class="btn btn-outline-danger" value="Poništi" name="btnReset" />
    <input type="submit" class="btn btn-outline-primary" value="Dodaj" name="btnSubmit" />
</form>