<?php 
        $res = $conn->query("select id, CONCAT(ime, ' ', prezime) as autor from autor order by id");
        $sviA = $res->fetchAll();

        $res = $conn->query("select * from zanr order by id");
        $sviG = $res->fetchAll();

        $res = $conn->query("select * from izdavac order by id");
        $sviP = $res->fetchAll();

        

?>


<form name="formAdd" method="POST" action="modules/add.php" enctype="multipart/form-data">
    <p class="text-primary">Dodavanje nove knjige</p>
    <div class="form-group">
        <input type="text" class="form-control name-100" name="tbName"
            placeholder="Unesite naziv" />
    </div>
    <div class="form-group">
        <input type="text" class="form-control" name="tbPrice"
            placeholder="Unesite cenu" />
    </div>
    <div class="form-group">
        <textarea class="form-control" placeholder="Unesite opis" name="taDesc" maxlength="1000"></textarea>
    </div>
    <form>
    <div class="form-group">
        <label for="fImg">Dodajte sliku knjige</label>
        <input type="file" class="form-control-file" id="fImg" name="fImg">
    </div>

    <div class="form-group">
        <input type="text" class="form-control" name="tbImgName"
            placeholder="Unesite ime slike" />
        <small class="form-text text-muted">
            Dozvoljena slova engleskog alfabeta, cifre i donja crta _
        </small>
    </div>

    <div class="form-group">
        <input type="text" class="form-control name-100" name="tbAlt"
            placeholder="Unesite opis slike" />
    </div>
    <div class="form-row form-group">
        <div class="col-md-6">
            <input type="number" class="form-control" name="tbYear" min="1960" max="<?= date("Y"); ?>" 
                placeholder="Godina izdanja" required/>
        </div>
        <div class="col-md-6">
            <input type="number" class="form-control" name="tbPages" min="1" max="99999"
                placeholder="Broj strana" required/>
        </div>
    </div>

    <div class="form-group">
      <label for="ddlPublisher">Izaberite izdavača </label>
      <select class="form-control" id="ddlPublisher" name="ddlPublisher">
        <?php foreach($sviP as $p) : ?>
            <option value="<?= $p->id?>" > <?= $p->naziv ?>  </option>
        <?php  endforeach; ?>
      </select>

      <label for="ddlGenre">Izaberite žanr knjige (može ih biti više)</label>
      <select multiple class="form-control" id="ddlGenre" name="ddlGenre[]">
        <?php $i = 1; foreach($sviG as $p) :  ?>
            <option value="<?=$p->id?>" <?php if($i) {echo "selected";} ?> > <?= $p->naziv ?>  </option>
        <?php $i = 0;  endforeach; ?>
      </select>

      <label for="ddlAuthor">Izaberite autora knjige (može ih biti više)</label>
      <select multiple class="form-control" id="ddlAuthor" name="ddlAuthor[]">
        <?php $i = 1; foreach($sviA as $p) : ?>
            <option value="<?= $p->id?>"  <?php if($i) {echo "selected";} ?> > <?= $p->autor ?>  </option>
        <?php $i = 0; endforeach; ?>
      </select>
    </div>


    <input type="hidden" name="what" value="books" />
    <input type="reset" class="btn btn-outline-danger" value="Poništi" name="btnReset" />
    <input type="submit" class="btn btn-outline-primary" value="Dodaj" name="btnSubmit" />
</form>