<form name="formAdd" method="POST" action="modules/add.php">
    <p class="text-primary">Dodavanje novog autora</p>
    <div class="form-group">
        <input type="text" class="form-control person-name"  name="tbFName"
            placeholder="Unesite ime" />
    </div>
    <div class="form-group">
        <input type="text" class="form-control person-name"  name="tbLName"
            placeholder="Unesite prezime" />
    </div>

    <input type="hidden" name="what" value="authors" />
    <input type="reset" class="btn btn-outline-danger" value="Poništi" name="btnReset" />
    <input type="submit" class="btn btn-outline-primary" value="Dodaj" name="btnSubmit" />
</form>

