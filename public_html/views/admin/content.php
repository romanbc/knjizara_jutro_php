<?php 
    if(isset($_SESSION['errAdd'])) {
        echo "<script>alert(\"" . $_SESSION['errAdd'] . "\")</script>";   
        unset($_SESSION['errAdd']);
    } 
    if(isset($_SESSION['sucAdd'])) {
        echo "<script>alert(\"" . $_SESSION['sucAdd'] . "\")</script>";   
        unset($_SESSION['sucAdd']);
    } 
    $name = $_SESSION['user']->ime;
    $tables = [
        ["books", "Knjige"],
        ["genres", "Žanrovi"],
        ["authors", "Autori"],
        ["publishers", "Izdavači"],
        ["users", "Korisnici"]
    ];
    $uri =  $_SERVER['REQUEST_URI'];
    $tab = strstr($uri, '?');
    $tab = substr($tab,1);
    $isUpdate = strpos($tab, '-');
    if($isUpdate) {
        $exp = explode("-", $tab);
        $id = $exp[0];
        $tab = $exp[1];
    }
    $all = !strpos($tab, '/');
?>

<div class="container my-2 my-lg-4 min-h">
    <div class="row pt-2" id="main">
        <div class="col-md-10 offset-md-1 text-center text-primary">
        <?php if(!$tab): ?><h2 >Zdravo <?= $name?></h2> <?php endif;?>
            <p class="text-info py-1">
                <?php  if($tab): require_once($tab . "/man.php");  else:   ?>
                    Izaberite tabelu iz baze čije podatke želite menjati , možete dodavati, menjati i brisati zapise. 
                <?php endif;  ?>
            </p>
            <ul class="nav justify-content-center py-4">
            <?php  foreach($tables as $table) : ?>
                <li class="nav-item m-1"><a class="btn btn-outline-primary <?= $tab ==  $table[0]  || $table[0] == explode('/',$tab)[0] ? "active" : "";?>" href="admin?<?=$table[0] ?>"><?=$table[1]?></a></li>
            <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 offset-lg-2">

        <?php  if ($tab && $all) 
        { echo ('<a class="btn btn-outline-primary my-2" href="admin?' . $tab . '/add">Dodaj</a>'); 
            require_once("functions.php"); require_once($tab . "/table.php");  }
            if(!$all) {  require_once($tab . "/form.php");  }
            
            ?>
        </div>
    </div>








</div>