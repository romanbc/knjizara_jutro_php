<form name="formAdd" method="POST" action="modules/add.php">
    <p class="text-primary">Dodavanje novog korisnika</p>
    <div class="form-group">
    <input type="text" class="form-control person-name"  name="tbFName"
        placeholder="Unesite ime" />
    </div>
    <div class="form-group">
        <input type="text" class="form-control person-name"  name="tbLName"
            placeholder="Unesite prezime" />
    </div>
    <div class="form-group">
        <input type="text" class="form-control"  name="tbMail"
            placeholder="Unesite e-mail" />
    </div>
    <div class="form-group">
        <input type="password" class="form-control"  name="tbPass"
            placeholder="Unesite lozinku" />
    <small class="form-text text-muted">
        Najmanje 6 znakova, makar jedno slovo, cifra i
        specijalni znak
    </small>
    </div>
    <div class="form-group">
        <label class="checkbox-inline mr-2"><input type="checkbox" value="1" name="chbActive" checked>Aktiviran</label> 
        <label class="radio-inline"><input type="radio" value="1" name="rbRole" checked>Korisnik</label>
        <label class="radio-inline"><input type="radio" value="2" name="rbRole">Administrator</label>
    </div>
        
    <input type="hidden" name="what" value="users" />
    <input type="reset" class="btn btn-outline-danger" value="Poništi" name="btnReset" />
    <input type="submit" class="btn btn-outline-primary" value="Dodaj" name="btnSubmit" />
</form>