<?php 
    $query = "select naziv from izdavac where id = $id";
    $result = $conn->query($query);
    $name = $result->fetch()->naziv;
?>

<form name="formAdd" method="POST" action="modules/update.php">
    <p class="text-primary">Izmena izdavača</p>
    <div class="form-group">
        <input type="text" class="form-control name-100"  name="tbName" value="<?= $name ?>"
            placeholder="Unesite naziv" />
    </div>

    <input type="hidden" name="what" value="publishers" />
    <input type="hidden" name="id" value="<?=$id?>" />
    <input type="reset" class="btn btn-outline-danger" value="Poništi" name="btnReset" />
    <input type="submit" class="btn btn-outline-primary" value="Izmeni" name="btnSubmit" />
</form>