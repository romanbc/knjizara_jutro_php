<?php
function printTable($ths, $trs, $what) {
    $table = '<table class="table table-responsive-lg text-center">
    <thead class="text-info"><tr><th>Id</th>';
    foreach($ths as $th) {
        $table .= '<th>' . $th . '</th>'; 
    }
    $table .= "<th>Izmeni</th> <th>Ukloni</th></tr></thead><tbody>";
    
    foreach($trs as $tr) {
        $table .= '<tr>';
        foreach($tr as $td) {
            $table .= '<td class="align-middle">' . $td . '</td>';
        }
        $id = $tr['id'];

        $table .= '
        <td class="align-middle">
            <a class="btn btn-outline-info my-2" href="admin?' . $id . '-' . $what . '/update">Izmeni</a>
        </td>
        <td class="align-middle text-danger">
            <form name="formDelete" method="POST" action="modules/delete.php" >
                <button type="submit" name="btnSubmit" class="btn btn-sm btn-outline-danger">
                    <i class="icon-remove"></i>
                </button>
                <input type="hidden" name="id" value="' . $id . '" />
                <input type="hidden" name="what" value="' . $what . '" />
            </form>
        </td></tr>';
        
    }
    $table .= '</tbody></table>';
    return $table;
}



?>