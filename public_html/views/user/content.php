<?php 
    
    
    $name = $_SESSION['user']->ime;
    $id = $_SESSION['user']->id;
    $res = $conn->query("select id from kupovina where id_korisnik = $id");
    if($res->rowCount()>0) {
    $idss = $res->fetchAll();
    $ids = [];
    foreach($idss as $id) {
        array_push($ids, $id->id);
    }
    $ids = implode(', ', $ids);
    $res = $conn->query("SELECT
    kd.kolicina,
    k.naziv,
    k.cena,
    kd.kolicina * k.cena as ukupno,
    kup.vreme_kupovine,
    s.src, s.alt
FROM
    kupovina_detalji kd
INNER JOIN knjiga k ON
    kd.id_knjiga = k.id
INNER JOIN kupovina kup ON
    kd.id_kupovina = kup.id 
    inner join slika s on k.id_slika = s.id  WHERE id_kupovina IN ($ids) order by 4 desc");
    $rows = $res->fetchAll();
    }
?>

<div class="container my-2 my-lg-4  min-h   ">
    <div class="row pt-2" id="main">
        <div class="col-md-10 offset-md-1 text-center">
         <h2 class="text-primary" >Zdravo <?= $name?></h2>
         <div id="current" class="m-4"></div>

        </div>
    </div>
    <div class="text-center">
        <h3 class="text-primary">Istorija kupovine </h3>
        <?php  if($rows):  ?>

        <table class="table table-responsive-lg text-center">
            <thead class="text-info">
                <tr>
                    <th></th>
                    <th>Knjiga</th>
                    <th>Cena</th>
                    <th>Količina</th>
                    <th>Ukupno</th>
                    <th>Vreme </th>
                </tr>
            </thead>
        <tbody>
        <?php foreach($rows as $r) : ?>
        <tr>
            <td class="align-middle">
                <img alt="<?=$r->alt?>" src="<?= substr($r->src, 1) ?>" width="60em">
            </td>
            <td class="align-middle text-primary">
                <?= $r->naziv?>
            </td>
            <td class="align-middle"> <?=$r->cena ?> RSD</td>
            <td class="align-middle" >
                <?=$r->kolicina ?>
            </td>
            <td class="align-middle text-primary"><?=$r->ukupno ?> RSD</td>
            <td class="align-middle"><?= date('Y-m-d h:i:s', $r->vreme_kupovine) ?></td>
        </tr>
<?php endforeach; ?>
    </tbody></table>
<?php else: echo "<p>Niste nikad ništa kupili kod nas</p>";  endif; ?>
    </div>
</div>