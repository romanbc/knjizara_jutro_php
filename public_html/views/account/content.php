

<div class="container my-2 my-lg-4">
<?php 
if(isset($_SESSION['greske'])) {
    echo $_SESSION['greske'];
}
if(isset($_SESSION['uspeh'])) {
    echo "<script>alert(\"" . $_SESSION['uspeh'] . "\")</script>";   
    unset($_SESSION['uspeh']);
}

//var_dump($_GET);

if(isset($_SESSION['user'])) {
    if($_SESSION['user']->naziv = "korisnik" ) {
        header("Location: ./user");
    }
}

?>
        <div class="row min-h" id="main">

            <div class="col-md-6 my-3">
                <form name="formSignUp" method="POST" action="modules/register.php">
                    <h2 class="text-primary">Registracija</h2>
                    <div class="form-group">
                        <label for="tbFNameSU">Ime</label>
                        <input type="text" class="form-control" id="tbFNameSU" name="tbFName"
                            placeholder="* Unesite ime" />
                        <!-- <small class="form-text text-muted">
                            Primer: Pera Perić
                        </small> -->
                    </div>
                    <div class="form-group">
                        <label for="tbLNameSU">Prezime</label>
                        <input type="text" class="form-control" id="tbLNameSU" name="tbLName"
                            placeholder="* Unesite prezime" />
                        <!-- <small class="form-text text-muted">
                            Primer: Pera Perić
                        </small> -->
                    </div>
                    <div class="form-group">
                        <label for="tbMailSU">E-mail</label>
                        <input type="text" class="form-control" id="tbMailSU" name="tbMail"
                            placeholder="* Unesite e-mail" />
                        <small class="form-text text-muted">
                            Primer: pera@gmail.com
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="tbPassSU">Lozinka</label>
                        <input type="password" class="form-control" id="tbPassSU" name="tbPass"
                            placeholder="* Unesite lozinku " />
                        <small class="form-text text-muted">
                            Najmanje 6 znakova, makar jedno slovo, cifra i
                            specijalni znak
                        </small>
                    </div>
                    <input type="reset" class="btn btn-outline-danger" value="Poništi" name="btnReset" />
                    <input type="submit" class="btn btn-outline-primary" value="Registracija" name="btnSubmit" />
                </form>
            </div>

            <div class="col-md-6 my-3">
                <form name="formSignIn" method="POST" action="modules/login.php">
                    <h2 class="text-primary">Prijava</h2>
                    <div class="form-group">
                        <label for="tbMailSI">E-mail</label>
                        <input type="text" class="form-control" id="tbMailSI" name="tbMail"
                            placeholder="Unesite Vaš e-mail" />
                    </div>
                    <div class="form-group">
                        <label for="tbPassSI">Lozinka</label>
                        <input type="password" class="form-control" id="tbPassSI" name="tbPass"
                            placeholder="Unesite Vašu lozinku" />
                    </div>
                    <input type="reset" class="btn btn-outline-danger" value="Poništi" name="btnReset" />
                    <input type="submit" class="btn btn-outline-primary" value="Prijava" name="btnSubmit" />
                </form>
            </div>
        </div>
    </div>
