<?php 

$menu = $conn->query("SELECT href, text FROM meni")->fetchAll(PDO::FETCH_ASSOC);

foreach ($menu as $m) :
?>
    <li class="nav-item <?= ($page) == $m['href'] || ($m['href'] == "./" && $page == "main") ? "active" : ""?>"><a class="nav-link font-weight-bold" href="
    <?= $m['href'] ?>"><?= $m['text'] ?></a></li>
<?php endforeach; ?>   