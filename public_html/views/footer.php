<div class="container-fluid bg-dark">
        <div class="container">
            <footer class="row py-3 text-light d-flex justify-content-between">
                <nav>
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="mailto:kontakt@knjizara.jutro.com" target="_blank"
                                aria-label="Mail us"><i class="icon-envelope"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="tel:+38111987654321" target="_blank" aria-label="Call us"><i
                                    class="icon-phone"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://goo.gl/maps/YSad3Vo7MBP2" target="_blank"
                                aria-label="Find us" rel="noreferrer"><i class="icon-map-marker"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.instagram.com/" target="_blank"
                                aria-label="Our instagram" rel="noreferrer"><i class="icon-instagram"></i></a>
                        </li>
                    </ul>
                </nav>
                <nav id="navFooter">
                    <ul class="nav text-right">
                        <?php require ("nav.php");  ?>
                    </ul>
                </nav>
            </footer>
        </div>
    </div>

    <button type="button" id="toTop" title="Na vrh">
        <i class="icon-arrow-up"></i>
    </button>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    
    <?php if ($page == "main") : ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script src="js/lazysizes.min.js"></script>
    <?php endif; ?>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
    crossorigin="anonymous"></script>
    <script src="js/common.min.js"></script>
    <?php if($page != "author") :?>
    <script src="js/<?=$page?>.min.js"></script>
    <?php endif; ?>


</body>

</html>