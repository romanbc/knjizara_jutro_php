<?php 

$id = $_GET['id'];

require_once ("../../setup/connection.php");


$b = $conn->query("

SELECT
    k.id,
    k.naziv,
    k.cena,
    k.opis,
    k.godina_izdanja,
    k.strana,
    i.naziv as izdavac,
    s.src,
    s.alt,
    GROUP_CONCAT(
        CONCAT(a.ime, ' ', a.prezime) SEPARATOR ', '
    ) AS autor,
    (
    SELECT
        GROUP_CONCAT(z.naziv SEPARATOR ', ') AS zanr
    FROM
        zanr z
    INNER JOIN knjiga_zanr kz ON
        z.id = kz.id_zanr
    INNER JOIN knjiga k1 ON
        k1.id = kz.id_knjiga
    WHERE
        k1.id = $id
    GROUP BY
        k1.id
) AS zanr
FROM
    knjiga k
INNER JOIN slika s ON
    k.id_slika = s.id
INNER JOIN knjiga_autor ka ON
    k.id = ka.id_knjiga
INNER JOIN autor a ON
    ka.id_autor = a.id
INNER JOIN izdavac i ON
    k.id_izdavac = i.id
WHERE
    k.id = $id
GROUP BY
    k.id






")->fetch();



$src = substr($b->src, 1);

?>





<div class="container">
    <div class="row">
        <div class="col-md-7 mb-0">
            <h5 class="text-uppercase text-primary">
            
                <?= $b->naziv  ?>
            
            </h5>
            <h6>
                <?= $b->zanr ?>
            </h6>
            <h6 class="text-success">
            
                <?= $b->autor ?>
            
            </h6>
            <p class="border-bottom mb-0 ">
            
                <?= $b->opis ?>
            
            
            
            </p>
            <div class="p-1 d-flex justify-content-between align-items-center">
                <div class="text-danger">
                
                <?= $b->cena ?> RSD
                
                
                </div>
                <div>
                <a href="#" data-id="
                    <?= $b->id ?>
                " 
                
                class="buy-details mx-1 btn btn-outline-success">Kupi</a>
                </div>
            </div>
        </div>
        <div class="col-md-5 order-md-first mt-0">
            <div>
                <ul class="list-group list-group-flush mb-2">
                    <li class="list-group-item p-1">Izdavač: 
                    <?= $b->izdavac ?>
                    
                    </li>
                    <li class="list-group-item p-1">Broj strana: 
                    
                    <?= $b->strana ?>

                    </li>
                    <li class="list-group-item p-1">Godina izdanja: 
                    
                    <?= $b->godina_izdanja ?>

                    </li>
                </ul>
            </div>
            <img class="img-fluid lazyload" 
      
            data-src="<?= $src ?>" 

            alt="<?= $b->alt ?>">
             
             
          
        </div>
    </div>
</div>


       