<?php 

    //var_dump($_GET);

    $params = "";

    if (isset($_GET['ddlGenre'])) {
        $genreId = $_GET['ddlGenre'];
        $params .= "&ddlGenre=$genreId";
    }
    if (isset($_GET['chbPublishers'])) {
        $publisherIds = $_GET['chbPublishers'];

        foreach($publisherIds as $pid) {
            $params .= "&chbPublishers[]=$pid";
        }

        $pubIdsString = implode(", ", $publisherIds);
    }
    if (isset($_GET['ddlSort'])) {
        $sort = $_GET['ddlSort'];
        $by = explode("-", $sort)[1];
        $dir = explode("-", $sort)[0];
        $params .= "&ddlSort=$sort";
    }
    $genres = $conn->query("select * from zanr")->fetchAll();

    $g = !empty($genreId) ? "INNER JOIN knjiga_zanr kz ON k.id = kz.id_knjiga WHERE kz.id_zanr = $genreId AND" : "WHERE" ;

    $queryPublishers = "SELECT i.id, i.naziv, COUNT(k.id) AS broj FROM izdavac i LEFT JOIN knjiga k ON i.id = k.id_izdavac  $g k.id IS NOT NULL  GROUP BY i.id order by 3 desc";
    
    $publishers = $conn->query($queryPublishers)->fetchAll();

    $pg = 0;

    if(isset($_GET['pg'])){
        $pg = ($_GET['pg'] - 1) * 6;
    }

    $queryBooks = "select k.id, k.naziv, k.cena, s.src, s.alt, GROUP_CONCAT(CONcat(a.ime, ' ', a.prezime) SEPARATOR ', ') as autor from knjiga k inner join slika s on k.id_slika = s.id inner join knjiga_autor ka on k.id = ka.id_knjiga inner join autor a on ka.id_autor = a.id "
    . ($genreId ?  " inner join knjiga_zanr kz on kz.id_knjiga = k.id " : " " ) .  ( $pubIdsString ?     " inner join izdavac i on k.id_izdavac = i.id " : " "  ) .  " WHERE "  . (  $genreId ? " kz.id_zanr = $genreId  " : " 1 " )  .     " AND "  .   ($pubIdsString ? " k.id_izdavac in($pubIdsString) " : " 1 ") . " GROUP by k.id"   .    ($by ?   " order by $by $dir "    :   " " ) .  "LIMIT $pg, 6" ;

    $res = $conn->query($queryBooks);    
    $books = $res->fetchAll();

    $queryNumOfBooks = "SELECT COUNT(DISTINCT k.id) AS num_of_books FROM knjiga k" . ($genreId ?  " inner join knjiga_zanr kz on kz.id_knjiga = k.id " : " " ) . ($pubIdsString ? " inner join izdavac i on k.id_izdavac = i.id" : "") .  "  WHERE "  . (  $genreId ? " kz.id_zanr = $genreId  " : " 1 " ) .
    " AND " . ($pubIdsString ? "k.id_izdavac in($pubIdsString)" : " 1 " );

    
    $num = $conn->query($queryNumOfBooks)->fetch()->num_of_books;
    $linkNum = ceil( $num / 6);
  
?>
    


<div class="container my-2 my-lg-4">
    <div class="row min-h" id="main">
        <div class="col-md-3">
            <nav class="navbar-expand-md p-1">
                <button class="navbar-toggler w-100 mb-1 text-primary font-weight-bold" type="button"
                    data-toggle="collapse" data-target="#navFilter">Prilagodi</button>
                <div class="list-group list-group-flush collapse navbar-collapse" id="navFilter">
                    <form method="GET" action="">
                    <div>
                        <div class="border-bottom">
                            <button class="list-group-item w-100 text-left text-primary btn-outline-light" type="button"
                                data-toggle="collapse" data-target="#card-filter-genre">
                                Žanr
                            </button>
                                <div class="collapse" id="card-filter-genre">
                                    <div class="card card-body border-0 mt-1 py-1">
                                        <select name="ddlGenre" class="form-control">
                                            <option value="0">SVI</option>
                                            <?php  foreach($genres as $g) :   ?>
                                            <option value="<?=$g->id?>" <?= $genreId == $g->id ? " selected" : ""  ?> ><?=$g->naziv?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                        </div>

                        <div class="border-bottom">
                            <button type="button"
                                class="list-group-item w-100 text-left text-primary btn-outline-light "
                                data-toggle="collapse" data-target="#card-filter-publisher">
                                Izdavač
                            </button>
                            <div class="collapse" id="card-filter-publisher">
                                <div class="card card-body border-0 mt-1 py-1">
                                    <div class="form-group" id="publishers">
                                        <?php  foreach($publishers as $p) :   ?>
                                        <div class="form-check">
                                            <input class="form-check-input  publisher" type="checkbox"
                                                name="chbPublishers[]" value="<?=$p->id?>" id="publisher-<?=$p->id?>" 
                                                
                                                <?php  if($publisherIds) echo  (in_array($p->id, $publisherIds) ? " checked" : "" ); ?>

                                                />
                                            <label class="form-check-label d-flex" for="publisher-<?=$p->id?>">
                                                <?=$p->naziv?><span class="ml-auto"> (<?=$p->broj?>)</span>
                                            </label>
                                        </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="border-bottom">
                            <button class="list-group-item w-100 text-left text-primary btn-outline-light"
                                data-toggle="collapse" data-target="#card-sort" type="button">
                                Sortiraj po
                            </button>
                                <div class="collapse" id="card-sort">
                                    <div class="card card-body border-0 mt-1 py-1">
                                    <select name="ddlSort" class="form-control">
                                        <option value="asc-2" >Nazivu A-Z</option>
                                        <option value="desc-2">Nazivu Z-A</option>
                                        <option value="asc-3" >Najjeftiniji </option>
                                        <option value="desc-3">Najskuplji</option>
                                     </select>
                                    </div>
                                </div>
                        </div>
                        <div class="mt-3">
                                <button type="reset" id="resetFilters" class="btn btn-outline-danger form-control m-1">
                                    Poništi
                                </button>
                                <button type="submit"  class="btn btn-outline-success form-control m-1">
                                    Primeni
                                </button>
                        </div>
                    </div>
                </form>
                </div>
            </nav>
        </div>
        <div class="col-md-9">
            <div  class="row">

                <?php if(count($books)):  foreach($books as $b): $src = substr($b->src, 1); ?>
                <div class="book mb-2 col-md-4 col-sm-6" data-aos="fade-up">
                    <div class="card border mx-md-0 mx-lg-1">
                        <img class="card-img-top lazyload" data-src="<?= $src ?>" alt="<?= $b->alt ?>">
                        <div class="card-body pt-0 text-center">
                            <h5 class="card-title text-primary text-uppercase"><?= $b->naziv ?></h5>
                            <h6 class="card-subtitle text-success"><?= $b->autor ?></h6>
                            <p class="my-1 text-danger"><?=  $b->cena ?> RSD</p>
                            <button type="button" class="btn btn-outline-primary details" data-toggle="modal"
                                data-target="#bookDetails" data-id="<?= $b->id ?>">Detalji</button>
                            <a href="#" data-id="<?= $b->id ?>" class="buy mx-1 btn btn-outline-success">Kupi</a>
                        </div>
                    </div>
                </div>
<?php  endforeach; else:  ?> <p class="text-info">Ne postoje knjige koje zadovoljavaju te kriterijume</h5> <?php endif; ?>
            </div>



            <ul class="nav nav-pills"> 

            <?php for($i=1; $i <= $linkNum; $i++): ?>
                <li><a href="?pg=<?=$i . $params?>" class="mx-2"><b> <?=$i?> </b></a></li>
            <?php endfor; ?>
            </ul>



        </div>
    </div>
</div>


<div class="modal fade" id="bookDetails" tabindex="-1">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="d-flex justify-content-between">
                <div></div>
                <button type="button" class="close p-3 p-md-2" data-dismiss="modal">
                    <span class="icon-close text-danger "></span>
                </button>
            </div>
            <div class="modal-body pt-0"></div>
        </div>
    </div>
</div>
