<?php
if(isset($_POST['btnSubmit'])) {
    session_start();    
    require_once ("../setup/connection.php");
    $id = $_POST['id'];
    $what = $_POST['what'];
    $table; 
    switch ($what) {
        case "authors": $table = "autor"; break;
        case "books": $table = "knjiga"; break;
        case "genres": $table = "zanr"; break;
        case "publishers": $table = "izdavac"; break;
        case "users": $table = "korisnik"; break;
    }

    require_once ("functions.php");

    if($table == "zanr" || $table == "autor") {
        deleteBookIfLeftWithoutGenreOrAuthor($table, $id);
    } else if($table == "izdavac") {
        deleteBookIfLeftWithoutPublisher($id);
    }
    
    if ($table == "knjiga") {
        deleteImgAndBook($id);
    } else {
        $query = "DELETE FROM  $table  WHERE id = $id ";
        $conn->query($query);
    }
  

    $_SESSION['sucAdd'] = "Uspesno obrisan $table";
    header("Location: ../admin?$what");
}


?>