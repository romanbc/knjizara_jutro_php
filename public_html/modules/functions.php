<?php 

    function ifNotRoleForbid($session, $role) {
        if(!isset($_SESSION[$session]) || $_SESSION[$session]->naziv != $role  ) {
            $_SESSION['forbidden'] = "Nemate pravo pristupa!";
            header("Location: ./");
        } 
    }

    function deleteImgAndBook($id_book) {
        global $conn;
        $res = $conn->query("SELECT s.id, src FROM slika s inner join knjiga k on s.id = k.id_slika   WHERE k.id = $id_book");
        $img = $res->fetch();
        $id_img = $img->id;
        $src = $img->src;
        if(file_exists($src)){
            unlink($src);
        }
        $conn->query("DELETE FROM slika WHERE id = $id_img");
    }

    function deleteBookIfLeftWithoutGenreOrAuthor($table, $id) {
        global $conn;
        $bookAffected;
        $query = "SELECT id_knjiga, (select count(id_$table) from knjiga_$table kveza where kveza.id_knjiga =  id_knjiga) as num FROM knjiga_$table WHERE id_$table = $id  GROUP BY id_knjiga";
        $res = $conn->query($query);
        $bookAffected = $res->fetch();
        if($bookAffected) {
            if($bookAffected->num == 1 ) {
                deleteImgAndBook($bookAffected->id_knjiga); 
            }
        }
    }

    function deleteBookIfLeftWithoutPublisher ($id) {
        global $conn;
        $booksAffected;
        $query = "SELECT id FROM knjiga WHERE id_izdavac = $id";
        $res = $conn->query($query);
        $booksAffected = $res->fetchAll();
        if($booksAffected) {
            foreach($booksAffected as $ba) {
                deleteImgAndBook($ba->id);
            }
        }
    }


?>