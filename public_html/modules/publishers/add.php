<?php 
    $name = $_POST['tbName'];

    $query = "INSERT INTO izdavac VALUES(null, :name)";
    $stmt = $conn->prepare($query);

    $stmt->bindParam(":name", $name);
    
    try{
        $success = $stmt->execute();

        if($success) {
            $_SESSION['sucAdd'] = "Uspesno dodat izdavac";
            header("Location: ../admin?" . $what);

        } else {
            $_SESSION['errAdd'] = "Interna greska servera";
            header("Location: ../admin?" . $what);
        }

    } catch (PDOException $e) {
        $_SESSION['errAdd'] = "Vec postoji izdavac sa tim nazivom";
        header("Location: ../admin?" . $what);
    }

?>