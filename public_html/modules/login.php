<?php 
    session_start();

    if(isset($_POST['btnSubmit'])) {
        $mail = $_POST['tbMail'];
        $pass = $_POST['tbPass'];
        $errors = [];
        $regPass= "/^(?=.*\d)(?=.*[A-zČĆŽŠĐčćžšđ])(?=.*[~!@#$%^&*<>?]).{6,}$/";
        if(!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            $erros[] = "Email nije ok";
        }
        if(!preg_match($regPass, $pass)) {
            $errors[] = "Lozinka nije ok";
        }
        if(count($errors) > 0) {
            $_SESSION['greske'] = $errors;
            header("Location: ../account");
        } else {
            require_once ("../setup/connection.php");
            $pass = md5($pass);
            $upit = "SELECT k.id, k.email, k.ime, u.naziv FROM korisnik k INNER JOIN uloga u ON k.id_uloga = u.id WHERE aktivan = 1
            AND email = :mail AND lozinka = :pass";
            $stmt = $conn->prepare($upit);
            $stmt->bindParam(":mail", $mail);
            $stmt->bindParam(":pass", $pass);
            $stmt->execute();
            $user = $stmt->fetch();
            if($user) {
                $_SESSION['user'] = $user;
                if($user->naziv == "korisnik") {

                    header("Location: ../user");
                } 
                if($user->naziv == "admin") {
                    header("Location: ../admin");
                }
            } else {
                $_SESSION['greske'] = "Pogresan email ili password ili niste verifikovali nalog";
                header("Location: ../account");
            }
        }
    }
?>