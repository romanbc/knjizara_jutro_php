<?php 

    $name = $_POST['tbName'];
    $desc = $_POST['taDesc'];
    $price = $_POST['tbPrice'];
    $year = $_POST['tbYear'];
    $pages = $_POST['tbPages'];
    $publisher = $_POST['ddlPublisher'];
    $genre = $_POST['ddlGenre'];
    $author = $_POST['ddlAuthor'];
    $alt = $_POST['tbAlt'];
    $imgName = $_POST['tbImgName'];
    $file= $_FILES['fImg'];
    $fileType = $file['type'];
    $allFormats = ["image/jpg", "image/jpeg", "image/png", "image/gif"];
    $id_pic = 0;
    $errs = [];

    if(!in_array($fileType,$allFormats)){
        array_push($errs, "Tip fajla nije validan. Dozvoljeni: jpg, jpeg, png, gif.");
    }
    $fileSize = $file['size'];

    if($fileSize > 3000000){
        array_push($errs, "Velicina fajla ne sme biti veca od 3MB!");
    }

    
    if(count($errs)==0){

        $tmpPath = $file['tmp_name'];
        $fileName = time()."_".$imgName.".".pathinfo($file['name'], PATHINFO_EXTENSION);
        $newPath = "../images/".$fileName;
        $moveSucc = move_uploaded_file($tmpPath, $newPath);
        if($moveSucc){
            //echo "<p class='alert alert-success'>Slika je upload-ovana na server!!</p>";
            $insert = "INSERT INTO slika VALUES (null, :src, :alt)";
            $stmt = $conn->prepare($insert);
            $stmt->bindParam(":alt", $alt);
            $stmt->bindParam(":src", $newPath);
            $result = $stmt->execute();
            if($result){
              //  echo "<p class='alert alert-success'>Putanja do slike i ostale informacije su upisane u bazu</p>";
                $id_pic = $conn->query("select id as last from slika where src = '$newPath'")->fetch()->last;
            } else {
                //echo "<p class='alert alert-danger'>Greska pri unosu podataka o slici u bazu!</p>";
            }

        } else {
            //echo "<p class='alert alert-danger'>Neuspeh pri upoad-ovanju slike na server!!</p>";
        }
    } else {
        echo "<ol>";
        foreach($errs as $err){
            echo "<li>$err</li>";
        }
        echo "</ol>";
    }

//var_dump($id_pic);

    $query = "INSERT INTO knjiga VALUES(null, :name, :desc, :price, :year, :pages,  $publisher, $id_pic) ";
    
    $stmt = $conn->prepare($query);

    //obrati paznju sta ima smisla proveravati, tj sta je korisnik unosio, rucno

    $stmt->bindParam(":name", $name);
    $stmt->bindParam(":desc", $desc);
    $stmt->bindParam(":price", $price);
    $stmt->bindParam(":year", $year);
    $stmt->bindParam(":pages", $pages);
    
    
    try{
        $success = $stmt->execute();
        if($success) {
            $_SESSION['sucAdd'] = "Uspesno dodata knjiga";
            $id_book = $conn->query("select id as last from knjiga where id_slika = $id_pic")->fetch()->last;

            //var_dump($genre);
            //var_dump($author);

            foreach($genre as $g) {
                $query = "INSERT INTO knjiga_zanr VALUES(null, " .  $id_book . ", " .  $g  .  ")";
                $conn->query($query);
            }

            foreach($author as $a) {
                $query = "INSERT INTO knjiga_autor VALUES(null, " .  $id_book . ", " .  $a  .  ")";
                $conn->query($query);
            }

            //echo "uspehhh";
            header("Location: ../admin?" . $what);
        } else {
            $_SESSION['errAdd'] = "Interna greska servera";
            header("Location: ../admin?" . $what);
        }

    } catch (PDOException $e) {
        $_SESSION['errAdd'] = "Vec postoji knjiga sa tom kombinacijom naziva, godine izdanja i izdavaca";
        
        //echo $e;
        header("Location: ../admin?" . $what);
    }








?>