<?php 

    $name = $_POST['tbName'];
    $desc = $_POST['taDesc'];
    $price = $_POST['tbPrice'];
    $year = $_POST['tbYear'];
    $pages = $_POST['tbPages'];
    $publisher = $_POST['ddlPublisher'];
    $genre = $_POST['ddlGenre'];
    $author = $_POST['ddlAuthor'];

    if(isset($_POST['chbChangeImg'])) {

        $alt = $_POST['tbAlt'];
        $imgName = $_POST['tbImgName'];
        $imgId = $_POST['imgId'];
        $file= $_FILES['fImg'];
        $fileType = $file['type'];
        $allFormats = ["image/jpg", "image/jpeg", "image/png", "image/gif"];
        $id_pic = 0;
        $errs = [];

        if(!in_array($fileType,$allFormats)){
            array_push($errs, "Tip fajla nije validan. Dozvoljeni: jpg, jpeg, png, gif.");
        }
        $fileSize = $file['size'];

        if($fileSize > 3000000){
            array_push($errs, "Velicina fajla ne sme biti veca od 3MB!");
        }

    
        if(count($errs)==0){

            $tmpPath = $file['tmp_name'];
            $fileName = time()."_".$imgName.".".pathinfo($file['name'], PATHINFO_EXTENSION);
            $newPath = "../images/".$fileName;
            $moveSucc = move_uploaded_file($tmpPath, $newPath);
            if($moveSucc){

                $res = $conn->query("SELECT src FROM slika WHERE id = $imgId");
                $img = $res->fetch();
                $src = $img->src;
                if(file_exists($src)){
                    unlink($src);
                }
                //echo "<p class='alert alert-success'>Slika je upload-ovana na server!!</p>";
                $query = "UPDATE slika SET src =  :src, alt =  :alt WHERE id = $imgId";
                $stmt = $conn->prepare($query);
                $stmt->bindParam(":alt", $alt);
                $stmt->bindParam(":src", $newPath);
                $result = $stmt->execute();
                if($result){
                //  echo "<p class='alert alert-success'>Putanja do slike i ostale informacije su upisane u bazu</p>";
                    //$id_pic = $conn->query("select last_insert_id() as last")->fetch()->last;
                } else {
                    //echo "<p class='alert alert-danger'>Greska pri unosu podataka o slici u bazu!</p>";
                }

            } else {
                //echo "<p class='alert alert-danger'>Neuspeh pri upoad-ovanju slike na server!!</p>";
            }
        } else {
            echo "<ol>";
            foreach($errs as $err){
                echo "<li>$err</li>";
            }
            echo "</ol>";
        }


    } 

    $query = "UPDATE knjiga SET naziv = :name, opis = :desc, cena  = :price, godina_izdanja = :year, strana = :pages, id_izdavac = $publisher WHERE id = $id";
    
    $stmt = $conn->prepare($query);

    //obrati paznju sta ima smisla proveravati, tj sta je korisnik unosio, rucno

    $stmt->bindParam(":name", $name);
    $stmt->bindParam(":desc", $desc);
    $stmt->bindParam(":price", $price);
    $stmt->bindParam(":year", $year);
    $stmt->bindParam(":pages", $pages);
    //$stmt->bindParam(":alt", $publisher); ovo npr ne mora proveravati
    
    try{
        $success = $stmt->execute();
        if($success) {
            
            $conn->query("DELETE FROM knjiga_zanr WHERE id_knjiga = $id");
            $conn->query("DELETE FROM knjiga_autor WHERE id_knjiga = $id");
            
            
            
            foreach($genre as $g) {
                $query = "INSERT INTO knjiga_zanr VALUES(null, " .  $id . ", " .  $g  .  ")";
                $conn->query($query);
            }
            
            foreach($author as $a) {
                $query = "INSERT INTO knjiga_autor VALUES(null, " .  $id  . ", " .  $a  .  ")";
                $conn->query($query);
            }

            
            $_SESSION['sucAdd'] = "Uspesno izmenjena knjiga";






            header("Location: ../admin?" . $what);
        } else {
            $_SESSION['errAdd'] = "Interna greska servera";
            header("Location: ../admin?" . $what);
        }

    } catch (PDOException $e) {

        $_SESSION['errAdd'] = "Vec postoji knjiga sa tom kombinacijom naziva, godine izdanja i izdavaca";
        //var_dump($e);
        
        header("Location: ../admin?" . $what);
    }








?>