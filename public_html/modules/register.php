<?php 
    session_start();

    require '../PHPMailer/src/PHPMailer.php';
    require '../PHPMailer/src/SMTP.php';
    require '../PHPMailer/src/Exception.php';

    
     use PHPMailer\PHPMailer\PHPMailer;
     use PHPMailer\PHPMailer\Exception;



    if(isset($_POST['btnSubmit'])) {



        //echo "nasao si me";
        $email = $_POST['tbMail'];
        $pass = $_POST['tbPass'];
        $fName = $_POST['tbFName'];
        $lName = $_POST['tbLName'];

        $errors = [];
        $regPass= "/^(?=.*\d)(?=.*[A-zČĆŽŠĐčćžšđ])(?=.*[~!@#$%^&*<>?]).{6,}$/";
        $regName = "/^[A-ZČĆŽŠĐ][a-zčćžšđ]{1,14}(\s[A-ZČĆŽŠĐ][a-zčćžšđ]{1,14})*$/";

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $erros[] = "Email nije ok";
        }

        if(!preg_match($regPass, $pass)) {
            $errors[] = "Lozinka nije ok";
        }

        if(!preg_match($regName, $fName)) {
            $errors[] = "Ime nije ok";
        }

        if(!preg_match($regName, $lName)) {
            $errors[] = "Prezime nije ok";
        }


        if(count($errors) > 0) {
            //http_response_code(422);
            //echo json_encode(["error" => $errors]);
            $_SESSION['greske'] = $errors;
            header("Location: ../account");
        } else {
            require_once ("../setup/connection.php");


            $pass = md5($pass);
            $time_reg = time();

            $upit = "INSERT INTO korisnik VALUES (null, :fName, :lName, :mail, :pass, " . $time_reg . ", 0, 1 )";
            $stmt = $conn->prepare($upit);
            $stmt->bindParam(":fName", $fName);
            $stmt->bindParam(":lName", $lName);
            $stmt->bindParam(":mail", $email);
            $stmt->bindParam(":pass", $pass);
            //$stmt->bindParam(":time_reg", $time_reg); //obrisi ovaj bindParam
            //$stmt->bindParam(":time_reg", $time_reg); nema potrebe da proveravam ovo sto sam sam napravio


            try {
                $uspeh = $stmt->execute();

                if($uspeh) {
                    $mail = new PHPMailer(true);

                    try {
                        $mail->SMTPDebug = 0; 
                        $mail->isSMTP();
                        $mail->Host = 'smtp.gmail.com';  
                        $mail->SMTPAuth = true;
                        $mail->Username = 'jutroknjizara@gmail.com';
                        $mail->Password = 'valjdaSad1';
                        $mail->SMTPSecure = 'tls';
                        $mail->Port = 587;
                        $mail->setFrom('jutroknjizara@gmail.com', 'Knjizara Jutro');
                        $mail->addAddress($email, ''. $fName . ' ' . $lName);
                        $mail->isHTML(true); 
                        $mail->Subject= "Verifikujte  nalog";
                        // $href =  "http://127.0.0.1/~ubu/web000PHP/modules/verify.php?t=" . $time_reg . "&m=" . $email; 
                        $href =  "https://knjizarajutro.000webhostapp.com/modules/verify.php?t=" . $time_reg . "&m=" . $email; 
                        $mail->Body = "Postovani , posetite sledeci <a href=\"" . $href .  "\">link</a> kako bi verifikovali vas nalog na sajtu knjizara jutro.";
                        $mail->send();
                    }
                    catch(Exception $e){
                        echo $e->getMessage();
                    }


                    $_SESSION['uspeh'] = "Uspesna registracija, poslat Vam je link za verifikaciju email-a";
                    header("Location: ../account");

                    
                } else {
                    $_SESSION['greske'] = "Interna greska servera";
                    header("Location: ../account");
                }

            } catch (PDOException $e) {
                $_SESSION['greske'] = "Vec postoji korisnik sa tom mail adresom";
                header("Location: ../account");
            }


        }
    }


?>