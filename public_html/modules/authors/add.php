<?php 
    $fName = $_POST['tbFName'];
    $lName = $_POST['tbLName'];

    $query = "INSERT INTO autor VALUES(null, :fName, :lName)";
    $stmt = $conn->prepare($query);

    $stmt->bindParam(":fName", $fName);
    $stmt->bindParam(":lName", $lName);
    
    try{
        $success = $stmt->execute();
        if($success) {
            $_SESSION['sucAdd'] = "Uspesno dodat autor";
            header("Location: ../admin?" . $what);

        } else {
            $_SESSION['errAdd'] = "Interna greska servera";
            header("Location: ../admin?" . $what);
        }

    } catch (PDOException $e) {
        $_SESSION['errAdd'] = "Vec postoji autor sa tom kombinacijom imena i prezimena";
        header("Location: ../admin?" . $what);
    }

?>