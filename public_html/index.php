<?php
  session_start();
  require_once("setup/connection.php");
  require_once("modules/functions.php");


  if(isset($_GET['page'])) {
    $page = $_GET['page'];
    switch($page) {
      case 'admin' : case  'account' : case 'author' : case 'cart' : case 'contact' : case 'user': break;
      default:  $page = "main";   
      break;
    }
  } else {
    $page = "main";
  }

  if(isset($_SESSION['forbidden'])) {
    echo "<script>alert(\"" . $_SESSION['forbidden'] . "\")</script>";   
    unset($_SESSION['forbidden']);
  } 


  if($page == "admin") {
        ifNotRoleForbid('user', 'admin');
  }

  if($page == "user") {
    ifNotRoleForbid('user', 'korisnik');
  }
  


  require_once("views/head.php");
  require_once("views/header.php");
  require_once("views/". $page . "/content.php");
  require_once("views/footer.php");

?>
