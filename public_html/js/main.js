/* eslint-disable no-undef */
$(document).ready(function() {
    AOS.init({
        duration: 700,
        easing: "ease-in-out",
        once: true
    });
    $(".buy").on("click", eventBuy);
    $("#bookDetails").on("show.bs.modal", eventDetails);
    $("#resetFilters").on("click", function() {
        window.location = "./";
    });
});

function eventDetails(e) {
    let bookId;
    bookId = $(e.relatedTarget).data("id");
    let thiss = $(this);

    $.ajax({
        url: "views/main/bookDetails.php?id=" + bookId,
        method: "GET",
        success: function(data) {
            $(thiss).find(".modal-body").html(data);
            $(".buy-details").on("click", eventBuyDetails);
        },
        error: function(xhr) {
            console.error(xhr.status);
        }
    });
}

function eventBuy(e) {
    e.preventDefault();
    let id = $(this).data("id");
    addToCart(id);
    toTop();
}

function eventBuyDetails(e) {
    e.preventDefault();
    let id = $(this).data("id");
    $("#bookDetails").modal("hide");
    addToCart(id);
    toTop();
}

function addToCart(id) {
    let books = getLS("books");
    if (books) {
        if (bookIsAlreadyInCart()) {
            updateQuantity();
        } else {
            addBookToLocalStorage();
        }
    } else {
        addFirstBookToLocalStorage();
    }

    printCartCount();

    function bookIsAlreadyInCart() {
        return books.filter(book => book.id == id).length;
    }

    function updateQuantity() {
        let books = getLS("books");
        for (let book in books) {
            if (books[book].id == id) {
                books[book].quantity++;
                break;
            }
        }
        setLS("books", books);
    }

    function addBookToLocalStorage() {
        let books = getLS("books");
        books.push({
            id: id,
            quantity: 1
        });
        setLS("books", books);
    }

    function addFirstBookToLocalStorage() {
        let books = [];
        books[0] = {
            id: id,
            quantity: 1
        };
        setLS("books", books);
    }
}

