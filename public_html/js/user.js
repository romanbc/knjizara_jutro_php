/* eslint-disable no-undef */
$(document).ready(function() {
    if (!hasLS("books")) {
        showEmptyCart();
    } else {
        booksCart = getLS("books");
        booksCartIds = booksCart.map(bc => bc.id);
        getBooksFromAjax();
    }
});

let booksCart, booksAjax, booksAjaxCart;

let booksCartIds;
function getBooksFromAjax() {
    $.ajax({
        url: "modules/ajaxCart.php",
        method: "POST",
        data: {
            ids:  booksCartIds,
            safe: "y"
        },
        success: function(data) {
            console.log(data);

            let pars = JSON.parse(data);
            booksAjax = [...pars];
            setBooksAjaxCart();
            totalPrice();
            $("#btnBuy").on("click", buyBooks);
            console.log(pars);
        }, 
        error: function(xhr) {
            console.error(xhr);
        }
    });


}

function setBooksAjaxCart() {
    booksAjaxCart = booksAjax.filter(bookA => {
        for (let bookLS of booksCart) {
            if (bookA.id == bookLS.id) {
                bookA.quantity = bookLS.quantity;
                return true;
            }
        }
        return false;
    });
}

function totalPrice() {
    let total = 0;
    for (let bookAjaxLS of booksAjaxCart) {
        for (let bookLS of booksCart) {
            if (bookAjaxLS.id == bookLS.id) {
                total +=
                    bookAjaxLS.cena * bookAjaxLS.quantity;
            }
        }
    }
    let html = `<div class="  text-center">
    <p>Trenutno je ukupan iznos Vaše korpe <span class="text-info">${total} RSD</span></p>
    <p>Da li želite da obavite kupovinu? </p>
    <button type="button" class="btn btn-outline-success" id="btnBuy">Da</button>
    </div>`;

    $("#current").append(html);
}


function buyBooks() {
    $.ajax(
        {
            url: "modules/ajaxBuy.php",
            method: "POST",
            data: {
                booksCart: booksCart,
                safe: "y"
            },
            success: function() {
                removeLS("books");
                printCartCount();
                alert("Uspešna kupovina");
                window.location = "";

            },
            error: function(xhr) {
                console.error(xhr);
            }
        }
    );
}